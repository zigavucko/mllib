__author__ = 'Ziga Vucko'


import numpy as np
from numpy.linalg import svd
import matplotlib.pyplot as plt


def build(p, k, auto_select_k, min_variance):
    # transpose from (rows: examples, columns: features) to (rows: features, columns: examples)
    p = p.T

    # calculate mean (center the data)
    m = np.mean(p, axis=1)
    means = np.tile(np.matrix(m).T, reps=(1, p.shape[1]))
    x = p - means

    # calculate dual covariance matrix
    dim = p.shape[0] - 1
    c = x.T.dot(x) / dim

    # SVD decomposition of dual covariance matrix
    u, s, v = svd(c)
    a = np.dot(x, u)
    s += 1e-15
    b = np.diag(1 / (dim * s))**0.5

    # calculate the basis of the eigenvector space
    u = a.dot(b)

    # choose number of PCA projection dimensions used according to given minimal boundary
    eigval_sum = sum(s)

    # automatic selection of appropriate k (defined by minimal variance of principal component)
    if auto_select_k:
        k_auto = 0
        for eigval in s:
            if (eigval / eigval_sum) > min_variance:
                k_auto += 1
            else:
                break
        k = k_auto
    else:
        # less eigenvalues than the number of desired principal components
        if p.shape[0] < k:
            k = p.shape[0]

    # explained variance
    # variance = [s[i] / eigval_sum for i in range(p.shape[0])] ?? TO-DO!! s.shape?!?
    variance = [s[i] / eigval_sum for i in range(s.shape[0])]

    # select principal components for the new feature subspace
    u = u[:, 1:k+1]

    return u, m, np.array(variance), k


def project(p, u, m):
    # transpose from (rows: examples, columns: features) to (rows: features, columns: examples)
    p = p.T

    # center the data
    means = np.tile(np.matrix(m).T, reps=(1, p.shape[1]))
    x = p - means

    # encode data set examples as projections into the PCA subspace and transpose back to (rows: examples, columns:
    # features)
    return u.T.dot(x).T


def plot(x, y, classes):
    n_classes = len(classes)

    ax = plt.subplot(111)
    signs = ['^', 's', 'o', 'x', '+', '*', '.', 'v', '>', '<'] * (n_classes/10 + 1)
    colors = ['blue', 'red', 'green', 'yellow', 'black', 'magenta', 'purple', 'pink', 'cyan', 'orange'] * (n_classes/10 + 1)
    for cl, marker, color in zip(range(n_classes), signs[:n_classes + 1], colors[:n_classes + 1]):
        plt.scatter(x=x[:, 0][y == cl],
                    y=x[:, 1][y == cl],
                    marker=marker,
                    color=color,
                    alpha=0.5,
                    label=classes[cl])

    plt.xlabel('PC1')
    plt.ylabel('PC2')

    leg = plt.legend(loc='upper right', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.title('PCA: projection onto the first 2 principal components')

    # hide axis ticks
    plt.tick_params(axis='both', which='both', bottom='off', top='off', labelbottom='on', left='off', right='off',
                    labelleft='on')

    # remove axis spines
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    plt.grid()
    plt.tight_layout
    plt.show()
