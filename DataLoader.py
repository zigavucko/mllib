__author__ = 'Ziga Vucko'


import numpy as np
import gzip
import csv
import json
import xml.etree.cElementTree as ET
import cPickle as pickle


def read_csv(f, delim, skip_legend, dtype):
    """
    Read csv file into a matrix.
    """
    # default delimiter is comma
    if delim is None:
        delim = ','

    reader = csv.reader(f, delimiter=delim)

    # skip legend (first row)
    if skip_legend:
        reader.next()

    # read file row by row and skip empty ones
    data = [row for row in reader if row]

    return np.array(data, dtype=dtype)


def read_json(f):
    """
    Read json file into python object.
    """
    return json.load(f)


def read_xml(f):
    """
    Read xml file into python object (return root of the tree).
    """
    tree = ET.parse(f)
    return tree.getroot()


def process_val(val, dtype):
    if dtype == unicode:
        return val.decode('utf-8')
    else:
        return dtype(val)


def read_txt(f, delim, skip_empty, comments, dtype):
    """
    Read txt file into a vector or a matrix.
    """
    # default delimiter is semicolon
    if delim is None:
        delim = ';'

    reader = f.read().splitlines()

    # one line vector (horizontal)
    if len(reader) == 1:
        if skip_empty:
            data = [process_val(val, dtype) for val in reader[0].split(delim) if val]
        else:
            data = [process_val(val, dtype) for val in reader[0].split(delim)]
    # multiple line vector (vertical) or matrix
    else:
        if skip_empty:
            data = [[process_val(val, dtype) for val in row.split(delim) if val] for row in reader if row and not row.startswith(comments)]
        else:
            data = [[process_val(val, dtype) for val in row.split(delim)] for row in reader if row and not row.startswith(comments)]

    data = np.array(data)

    # trim empty lines (for vector, not matrix)
    try:
        # row vector (numpy)
        if data.shape[0] == 1:
            data = data[0, :]
        # column vector (numpy)
        elif data.shape[1] == 1:
            data = data[:, 0]
    except IndexError:
        pass

    return data


def read_data(f, delim, skip_legend, comments, dtype):
    """
    Read data file into a vector or a matrix.
    """
    # default delimiter is space
    if delim is None:
        delim = ' '

    return np.loadtxt(f, delimiter=delim, skiprows=skip_legend, comments=comments, dtype=dtype)


def load_nonnumpy_object(file_name):
    """
    Read non-numpy object (list/dict/set).
    """
    f = open(file_name, 'r')
    data = pickle.load(f)
    f.close()
    return np.array(data, dtype=object)


def load(file_name, delim=None, skip_legend=False, skip_empty=True, comments='#', dtype=float):
    """
    Function loads the file, reads its contents and puts it into a vector or a matrix. Supported files with extensions
    are csv, csv.gz, json, json.gz, xml, xml.gz, txt, txt.gz, data, data.gz.
    :param file_name: Absolute or relative path to file that will be read.
    :param delim: Delimiter used when dividing the contents in a single row.
    :param skip_legend: If true, first line of the file read is skipped (regarded as legend).
    :param skip_empty: If true, empty values in the file read are skipped (not added to the vector/matrix).
    :param comments: Character denoting the start of comment row. If such character is found as the first one in a row,
    then row is skipped.
    :param dtype: Data type to which the contents of a vector or a matrix should be converted to. If numeric, then no
    strings should be part of the contents.
    :return: A vector or a matrix (numpy) containing data contents.
    """

    if dtype == object:
        return load_nonnumpy_object(file_name)

    # open the file according to the extension
    extensions = file_name.split('.')

    # extension
    try:
        # known extension
        if extensions[-1] == 'gz':
            f = gzip.open(file_name, 'rb')
        elif extensions[-1] in ['csv', 'json', 'xml', 'txt', 'data']:
            f = open(file_name, 'rb')
        # arbitrary extension
        else:
            return None
    # no extension
    except IndexError:
        return None

    # read and parse the contents of the file according to its extension
    data = np.array([])

    # csv, csv.gz
    if extensions[-1] == 'csv' or (extensions[-2] == 'csv' and extensions[-1] == 'gz'):
        data = read_csv(f, delim, skip_legend, dtype)

    # json, json.gz
    elif extensions[-1] == 'json' or (extensions[-2] == 'json' and extensions[-1] == 'gz'):
        data = read_json(f)

    # xml, xml.gz
    elif extensions[-1] == 'xml' or (extensions[-2] == 'xml' and extensions[-1] == 'gz'):
        data = read_xml(f)

    # txt, txt.gz
    elif extensions[-1] == 'txt' or (extensions[-2] == 'txt' and extensions[-1] == 'gz'):
        data = read_txt(f, delim, skip_empty, comments, dtype)

    # data, data.gz
    elif extensions[-1] == 'data' or (extensions[-2] == 'data' and extensions[-1] == 'gz'):
        data = read_data(f, delim, skip_legend, comments, dtype)

    f.close()

    return data
