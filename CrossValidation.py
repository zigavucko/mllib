__author__ = 'Ziga Vucko'


import numpy as np


def indices(iteration, k, length):
    """
    Calculate training set and test set intervals.
    """
    # sequence of indices
    seq = range(length)

    # average chunk length
    avg_len = float(length) / k

    # placeholder for intervals of indices
    intervals = []

    # assign correct indices to each of k intervals
    idx = 0.0
    while idx < length:
        intervals.append(seq[int(idx):int(idx + avg_len)])
        idx += avg_len

    # determine training and test set intervals
    indices_train = sum([intervals[i] for i in range(k) if i != iteration], [])
    indices_test = intervals[iteration]

    return np.array(indices_train), np.array(indices_test)


def divide(x, y, k=5):
    """
    Divide the original training set into k parts and return the new training and test set according to the 1:k ratio.
    """
    for i in range(k):
        indices_train, indices_test = indices(i, k, x.shape[0])
        x_train = x[indices_train, :]
        x_test = x[indices_test, :]
        y_train = y[indices_train]
        y_test = y[indices_test]
        yield x_train, x_test, y_train, y_test


def divide_range(n, k=5):
    """
    Divide the original range specified by n into k parts and return the current training indices and current test set
    indices according to the 1:k ratio.
    """
    for i in range(k):
        indices_train, indices_test = indices(i, k, n)
        yield indices_train, indices_test
