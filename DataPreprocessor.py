__author__ = 'Ziga Vucko'


import numpy as np
from scipy.sparse import csr_matrix, hstack
from sklearn.preprocessing import LabelEncoder
import sklearn.utils


def standardize(x):
    """
    Standardization or Z-score normalization. We rescale features (columns) of the dataset x, so that they have the
    properties of a standard normal distribution (mean = 0, standard deviation = 1)
    """
    means = np.mean(x, axis=0)
    stds = np.std(x, axis=0)
    return (x - means) / stds


def normalize(x, feature_range=(0, 1)):
    """
    Normalization or min-max scaling. We rescale the features to unit range, typically to a range between 0 and 1.
    """
    x_min = x.min(axis=0)
    x_max = x.max(axis=0)
    min_ = feature_range[0]
    max_ = feature_range[1]
    return ((x - x_min) / (x_max - x_min)) * (max_ - min_) + min_


def encode_labels(y_true, y_pred=None):
    # if string array, encode labels
    if str(y_true.dtype)[1] in ['S', 'U']:
        encoder = LabelEncoder()
        encoder.fit(y_true)
        y_true = encoder.transform(y_true)
        classes = dict(enumerate(list(encoder.classes_)))
        if y_pred is not None:
            y_pred = encoder.transform(y_pred)
            return y_true, y_pred, classes
        else:
            return y_true, classes
    # if labels are already numeric, just return them back
    else:
        classes = dict(enumerate(list(set(y_true))))
        if y_pred is not None:
            return y_true, y_pred, classes
        else:
            return y_true, classes


def shuffle_data(x, y):
    try:
        # sparse matrix
        x.getformat()
        mat = hstack((x, csr_matrix(y).T))
        mat = sklearn.utils.shuffle(mat)
        y = mat[:, -1].T.toarray()[0].astype(int)
    except AttributeError:
        # dense matrix
        mat = np.column_stack((x, y))
        np.random.shuffle(mat)
        y = mat[:, -1].astype(int)

    x = mat[:, :-1]

    return x, y


def separate_dataset(x, y, ratio=0.67, shuffle=False):
    """
    Separate the data set (x and y) into training and test set (default ratio = 0.67 : 0.33). Optionally data set can
    be shuffled before division.
    """
    if shuffle:
        x, y = shuffle_data(x, y)

    bound = int(ratio * x.shape[0])
    x_train = x[:bound, :]
    x_test = x[bound:, :]
    y_train = y[:bound]
    y_test = y[bound:]

    return x_train, x_test, y_train, y_test
