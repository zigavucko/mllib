__author__ = 'Ziga Vucko'


import FeatureMatrix
import CrossValidation
import Evaluator
import numpy as np


def predict(train, test, learners, classifiers):
    # empty matrix suitable for column stacking (predictions)
    y_pred = FeatureMatrix.create(test.shape[0], 0)

    # no classifiers have been passed to the function, so we have to learn the models first
    if not classifiers:
        for learner in learners:
            classifier = learner(train)
            predictions = classifier(test, remove_y=True)
            y_pred = np.column_stack((y_pred, predictions))
    # classifiers have been passed to the function, so we can directly predict
    else:
        for classifier in classifiers:
            predictions = classifier(test, remove_y=True)
            y_pred = np.column_stack((y_pred, predictions))

    # append vector of true values to the temporary matrix of predictions
    y_true = test[:, -1]
    temp = np.column_stack((y_pred, y_true))

    return temp


def run(data, learners=[], classifiers=[], k=5, cv=False, save_matrix=False):
    """
    Build a matrix composed of predictions of given classifiers using the cross-validation technique
    or fixed ratio to divide the dataset into training and test subsets.
    """
    # run stacking algorithm using cross-validation technique
    if cv:
        # empty matrix suitable for row stacking (general)
        x = FeatureMatrix.create(0, len(learners) + 1)
        for train, test, i in CrossValidation.divide(data, k):
            # predict the results with all given classifiers and append temporary matrix to the general matrix
            temp = predict(train, test, learners, classifiers)
            x = np.vstack((x, temp))

        suffix = 'cv%d' % k

    # run stacking algorithm using fixed training-test set ratio
    else:
        # divide training and test set according to the parameter k (ratio: size(train) = k * size(test))
        bound = np.floor(data.shape[0] / k)
        train = data[:bound]
        test = data[bound:]

        # predict the results with all given classifiers
        x = predict(train, test, learners, classifiers)

        suffix = 'fixed%d' % k

    # optionally save matrix used which will be used for building stacking classifier
    if save_matrix:
        FeatureMatrix.save('data/matrices/matrix.stacking.%s' % suffix, x)

    return x


def evaluate_predictions(x, measure='mae'):
    """
    Evaluate prediction matrix by computing the errors of predictions (one column per algorithm).
    """
    # get pointer to the metric function
    metric = Evaluator.get_metric(measure)

    # extract true values of predictions
    y_true = x[:, -1]

    # placeholder for errors
    errors = []

    # iterate through all columns except the last one and calculate the error of predictions
    for i in range(x.shape[1] - 1):
        y_pred = x[:, i]
        errors.append(metric(y_true, y_pred))

    return errors


def predict_on_test_set(x, classifiers, classifier_stacking):
    """
    Predict results on the test set using base classifiers and stacking classifier.
    """
    # empty matrix suitable for column stacking (predictions)
    y_pred = FeatureMatrix.create(x.shape[0], 0)

    # predict results using base classifiers
    for classifier in classifiers:
        predictions = classifier(x, remove_y=False)
        y_pred = np.column_stack((y_pred, predictions))

    # predict results using stacking classifier
    predictions = classifier_stacking(y_pred)

    return predictions