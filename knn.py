__author__ = 'Ziga Vucko'


from collections import defaultdict, OrderedDict
import numpy as np
from scipy.spatial.distance import cityblock, euclidean, sqeuclidean, canberra, chebyshev, cosine, hamming


def get_distance_function(measure):
    if measure == 'manhattan':
        return cityblock
    elif measure == 'euclidean':
        return euclidean
    elif measure == 'sqeuclidean':
        return sqeuclidean
    elif measure == 'canberra':
        return canberra
    elif measure == 'chebyshev':
        return chebyshev
    elif measure == 'cosine':
        return cosine
    elif measure == 'hamming':
        return hamming


def get_neighbors(x, example, k, measure):
    measure_func = get_distance_function(measure)
    distances = {i: measure_func(x[i, :], example) for i in range(x.shape[0])}

    return OrderedDict(sorted(distances.items(), key=lambda a: a[1])).keys()[:k]


def get_response(y, neighbors):
    class_dtype = str(y.dtype)

    # classification
    if class_dtype.startswith('int') or class_dtype.startswith('|S'):
        votes = defaultdict(int)
        for n in neighbors:
            votes[y[n]] += 1
        return OrderedDict(sorted(votes.items(), key=lambda a: a[1], reverse=True)).keys()[0]

    # regression
    elif class_dtype.startswith('float'):
        return np.mean(y[neighbors, :])


def predict(x_train, y_train, x_test, k, measure):
    # predict classes for the test set examples
    y_pred = []
    for i in range(x_test.shape[0]):
        # get indices of k nearest neighbors
        neighbors = get_neighbors(x_train, x_test[i, :], k=k, measure=measure)
        # get predicted response based on the k nearest neighbors
        response = get_response(y_train, neighbors)
        y_pred.append(response)

    return np.array(y_pred)
