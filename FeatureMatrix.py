__author__ = 'Ziga Vucko'


from os import path, makedirs
import numpy as np
from scipy.sparse import csr_matrix
import cPickle as pickle


def append_ones(x):
    """
    Append a column of ones to the left-hand side of the matrix (or a vector).
    """
    try:
        return np.hstack((np.ones((x.shape[0], 1)), x))
    except AttributeError:
        return np.hstack((1, x))


def load(file_name, dense=True):
    """
    Load the matrix from a binary file.
    """
    f = open(file_name, 'rb')
    x = pickle.load(f)
    f.close()

    if dense:
        x = x.toarray()

    return x


def save(file_name, x):
    """
    Save the matrix to a binary file.
    """
    # if directory doesn't exist, create it
    dir_name = file_name[0:file_name.rfind('/')]
    if not path.isdir(dir_name):
        makedirs(dir_name)

    # convert the matrix to sparse
    x = csr_matrix(x)

    f = open(file_name, 'wb')
    pickle.dump(x, f, pickle.HIGHEST_PROTOCOL)
    f.close()
