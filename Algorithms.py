__author__ = 'Ziga Vucko'


import numpy as np
from mllib import knn, pca, Evaluator


##########################################################################
# KNN (k-Nearest Neighbors)
##########################################################################
class KNN:
    def __init__(self, **kwargs):
        """
        Constructor of KNN model class. Declares and initializes the variables to their default values.
        :param kwargs: Custom model parameters given as keyworded arguments.
        :return: KNN model (self).
        """
        self.k = kwargs.get('k', 5)
        self.measure = kwargs.get('measure', 'sqeuclidean')
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None
        self.y_pred = None

    def fit(self, x, y):
        """
        Function builds a KNN model based on training set data.
        :param x: Data set containing training examples (rows: examples, columns: features).
        :param y: Vector of class labels of training set examples.
        :return: KNN model (self).
        """
        self.x_train = x
        self.y_train = y

        return self

    def predict(self, x, y):
        """
        Function predicts class labels for test set data.
        :param x: Data set containing test examples (rows: examples, columns: features).
        :param y: Vector of class labels of test set examples.
        :return: Vector of predicted class labels.
        """
        self.x_test = x
        self.y_test = y
        self.y_pred = knn.predict(self.x_train, self.y_train, self.x_test, self.k, self.measure)

        return self.y_pred

    def fit_predict(self, x_train, y_train, x_test, y_test):
        """
        Function builds a KNN model based on training set data and predicts class labels for test set data.
        :param x_train: Data set containing training examples (rows: examples, columns: features).
        :param y_train: Vector of class labels of training set examples.
        :param x_test: Data set containing test examples (rows: examples, columns: features).
        :param y_test: Vector of class labels of test set examples.
        :return: Vector of predicted class labels.
        """
        self.fit(x_train, y_train)
        return self.predict(x_test, y_test)

    def eval(self, measure='ca', log=True):
        """
        Function evaluates the results of the prediction.
        :param measure: String specifying the function used for calculating the quality of classification results.
        :param log: Whether or not the result should be printed automatically.
        :return: If log is true, then the result returned from calling the measure function is returned.
        """
        metric_function = Evaluator.get_metric_func(measure)
        return metric_function(self.y_test, self.y_pred, log)
##########################################################################


##########################################################################
# PCA (Principal Component Analysis)
##########################################################################
class PCA:
    def __init__(self, k=5, auto_select_k=False, min_variance=0.01):
        """
        Constructor of PCA classifier class. Declares and initializes the variables to their default values.
        :param k: Number of dimensions used in the new PCA feature subspace.
        :param auto_select_k: If true, then algorithm will automatically keep only those eigenvectors and their
        eigenvalues, whose share of described variance is greater than min_variance.
        :param min_variance: Used for automatic selection of parameter k. It is relevant only if auto_select_k is true.
        :return: PCA classifier (self).
        """
        self.k = k
        self.auto_select_k = auto_select_k
        self.min_variance = min_variance

        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None
        self.y_pred = None

        self.u = None
        self.means = None
        self.variance = None

        self.x_train_projected = None
        self.x_test_projected = None

    def fit(self, x, y):
        """
        Function builds a PCA model based on training set data and projects it into the PCA subspace for further use.
        :param x: Data set containing training examples (rows: examples, columns: features).
        :param y: Vector of class labels of training set examples.
        :return: PCA classifier (self).
        """
        self.x_train = x
        self.y_train = y

        # build PCA model
        self.u, self.means, self.variance, self.k = pca.build(self.x_train, self.k, self.auto_select_k, self.min_variance)

        # project the training set into PCA subspace
        self.x_train_projected = self.project(self.x_train)

        return self

    def predict(self, x, y, **kwargs):
        """
        Function predicts class labels for test set data using the KNN classifier. Before prediction the test set data
        is projected into the PCA subspace.
        :param x: Data set containing test examples (rows: examples, columns: features).
        :param y: Vector of class labels of test set examples.
        :param kwargs: Custom parameters given as keyworded arguments. Passed on to KNN classifier.
        :return: Vector of predicted class labels.
        """
        self.x_test = x
        self.y_test = y

        # project the test set into PCA subspace
        self.x_test_projected = self.project(self.x_test)

        # default prediction function is KNN with k=5 and measure='sqeuclidean' or prediction with custom parameters
        # given as keyworded parameters
        self.y_pred = KNN(**kwargs).fit_predict(self.x_train_projected, self.y_train, self.x_test_projected, self.y_test)

        return self.y_pred

    def fit_predict(self, x_train, y_train, x_test, y_test, **kwargs):
        """
        Function builds a PCA model based on training set data and predicts class labels for test set data using the KNN
        classifier. Before prediction the test set data is projected into the PCA subspace.
        :param x_train: Data set containing training examples (rows: examples, columns: features).
        :param y_train: Vector of class labels of training set examples.
        :param x_test: Data set containing test examples (rows: examples, columns: features).
        :param y_test: Vector of class labels of test set examples.
        :param kwargs: Custom parameters given as keyworded arguments. Passed on to KNN classifier.
        :return: Vector of predicted class labels.
        """
        self.fit(x_train, y_train)
        return self.predict(x_test, y_test, **kwargs)

    def project(self, x):
        """
        Function projects the data set into the PCA subspace.
        :param x: Data set to be projected (rows: examples, columns: features).
        :return: Data set projected into the PCA subspace (shape=(n_examples, k)).
        """
        return pca.project(x, self.u, self.means)

    def plot(self, which='model'):
        """
        Function plots points in the new PCA subspace (first two principal components).
        :param which: 2 options for plotting the data: model (original labels used in training set) / prediction
        (labels predicted on the test set).
        :return: None
        """
        classes = np.array(list(set(self.y_train)), dtype=int)
        if which == 'model':
            pca.plot(self.x_train_projected, self.y_train, classes)
        elif which == 'prediction':
            pca.plot(self.x_test_projected, self.y_pred, classes)

    def eval(self, measure='ca', log=True):
        """
        Function evaluates the results of the prediction.
        :param measure: String specifying the function used for calculating the quality of classification results.
        :param log: Whether or not the result should be printed automatically.
        :return: If log is true, then the result returned from calling the measure function is returned.
        """
        metric_function = Evaluator.get_metric_func(measure)
        return metric_function(self.y_test, self.y_pred, log)
##########################################################################
