__author__ = 'Ziga Vucko'


from os.path import basename
from sys import argv
import time
import numpy as np
from scipy.optimize import fmin_l_bfgs_b
from mllib import FeatureMatrix


class LinearRegressionLearner:
    def __init__(self, param=0):
        self.param = param
        self.model = None

    def _cost_grad_linear(self, theta, x, y):
        """
        Regularization function.
        """
        m = x.shape[0]
        theta1 = theta[1:]

        t = x.dot(theta) - y
        j = (t.dot(t) + self.param * theta1.dot(theta1)) / (2. * m)

        grad = x.T.dot(t) / m
        grad[1:] += self.param * theta1 / m

        return j, grad

    def __call__(self, x_train, y_train):
        """
        Build a Linear regression model and return theta parameters.
        """
        x_train = FeatureMatrix.append_ones(x_train)
        self.model = fmin_l_bfgs_b(func=self._cost_grad_linear,
                                   x0=np.zeros(x_train.shape[1]),
                                   args=(x_train, y_train))[0]

        return LinearRegressionPredictor(self.model)


class LinearRegressionPredictor:
    def __init__(self, model):
        self.model = model

    def __call__(self, x_test):
        """
        Predict the y value of a matrix or of a single vector.
        """
        x_test = FeatureMatrix.append_ones(x_test)

        return x_test.dot(self.model)


def main():
    x_train = np.array([[13773], [14569]], dtype=float)
    y_train = np.array([1184, 1574], dtype=float)
    x_test = np.array([[15369]], dtype=float)

    learner = LinearRegressionLearner()
    clf = learner(x_train, y_train)
    y_pred = clf(x_test)
    print y_pred


if __name__ == '__main__':
    t0 = time.time()
    print
    main()
    print
    print '%s executed successfully in %.2f s.' % (basename(argv[0]), time.time() - t0)
