__author__ = 'Ziga Vucko'


from os import path, makedirs
import errno
from sklearn.externals import joblib


def make_dirs(path):
    try:
        makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def save(file_name, model):
    """
    Save prediction model to a file in the binary format.
    """
    # if directory doesn't exist, create it
    idx = file_name.rfind('/')
    if idx > 0:
        make_dirs(file_name[:idx])

    joblib.dump(model, file_name)


def load(file_name):
    """
    Load prediction model from a file in the binary format.
    """
    return joblib.load(file_name)
