__author__ = 'Ziga Vucko'


import numpy as np


def polynomial_vector(a, b, scale_a=1.0, scale_b=1.0, k=5):
    """
    Create a vector filled with all combinations of products of two given values, where these values are
    raised to the power of i in given iteration all the way to the power of k.
    """
    a /= scale_a
    b /= scale_b
    vector = [a, b]
    for i in range(1, k+1):
        for j in range(1, i+1):
            vector += [a**i * b**j]
            if i != j:
                vector += [a**j * b**i]

    return np.array(vector)


def binary_vector(length, index):
    """
    Create a binary vector of given length. Assign the value given index to 1.
    """
    vector = np.zeros(length)
    if index >= 0:
        vector[index] = 1.0
    return vector


def binary_interval_vector(n_features, key=False):
    """
    Create feature interval indices vector for feature matrix building process.
    """
    if key:
        n_features = [1] + n_features
    vector = []
    for i in range(len(n_features) + 1):
        vector += [sum(n_features[0:i])]

    return vector