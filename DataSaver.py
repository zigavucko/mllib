__author__ = 'Ziga Vucko'


from os import path, makedirs
import errno
import cPickle as pickle
import json
import numpy as np


def save_json(f, x):
    """
    Write json object to file.
    """
    return json.dump(x, f)


def save_html(f, x):
    """
    Write string representing html structure to file.
    """
    f.write(x.encode('utf-8'))


def save_nonnumpy_object(f, x):
    """
    Write non-numpy Python object to file.
    """
    pickle.dump(x, f)


def save_vector(f, vec, delim):
    """
    Write vector to file. Values delimited by char specified by variable delim.
    """
    for i, val in enumerate(vec):
        if isinstance(val, str):
            f.write(val)
        elif isinstance(val, unicode):
            f.write(val.encode('utf-8'))
        else:
            f.write(str(val))
        if i < len(vec)-1:
            f.write(delim)


def save_matrix(f, mat, delim, top, left):
    """
    Write matrix to file. Values delimited by char specified by variable delim.
    """
    # top legend
    if top:
        for i in range(len(top)):
            f.write(str(top[i]))
            if i < len(top)-1:
                f.write(delim)
        f.write('\n')

    for i in range(len(mat)):
        # left-side legend
        if left:
            f.write(str(left[i]) + delim)

        # actual data
        for j in range(len(mat[i])):
            f.write(str(mat[i][j]))
            if j < len(mat[i])-1:
                f.write(delim)

        # new line
        f.write('\n')


def make_dirs(path):
    try:
        makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def save(file_name, x, delim=';', top=None, left=None):
    """
    Function saves the contents of a vector or a matrix or a non-numpy Python object specified by variable x to a file
    specified by variable file_name.
    """
    # if directory doesn't exist, create it
    idx = file_name.rfind('/')
    if idx > 0:
        make_dirs(file_name[:idx])

    f = open(file_name, 'wb')

    # json (also for dict)
    if file_name.endswith('.json'):
        save_json(f, x)

    # html
    elif file_name.endswith('.html'):
        save_html(f, x)

    # object (non-numpy)
    elif x.dtype.type == np.object_:
        save_nonnumpy_object(f, x)

    else:
        try:
            x.shape[1]
            # matrix (numpy)
            save_matrix(f, x, delim, top, left)
        except IndexError:
            # vector (numpy)
            save_vector(f, x, delim)
        except AttributeError:
            # object (non-numpy)
            save_nonnumpy_object(f, x)

    f.close()
