import datetime


FORMAT = ['%Y-%m-%d %H:%M:%S.%f', '%Y-%m-%d']


def date_parse(d, f=0):
    if not isinstance(d, datetime.datetime):
        d = datetime.datetime.strptime(d, FORMAT[f])
    return d


def date_diff(d1, d2):
    return (date_parse(d1) - date_parse(d2)).total_seconds()


def date_add(d1, seconds, f=0):
    d2 = datetime.timedelta(seconds=seconds)
    d = date_parse(d1) + d2
    return d.strftime(FORMAT[f])
