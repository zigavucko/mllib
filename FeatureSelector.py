__author__ = 'Ziga Vucko'


from collections import OrderedDict
from itertools import islice
import numpy as np
from sklearn.ensemble import ExtraTreesClassifier


def select_features(x, y):
    clf = ExtraTreesClassifier(n_estimators=10)
    x_new = clf.fit(x, y).transform(x)

    # indices of redundant features
    features = dict(list(enumerate(clf.feature_importances_)))
    features = OrderedDict(sorted(features.items(), key=lambda a: a[1], reverse=True))
    redundant = OrderedDict(islice(features.items(), x_new.shape[1], x.shape[1])).keys()

    return x_new, redundant


def remove_features(x, redundant):
    return np.delete(x, redundant, axis=1)
