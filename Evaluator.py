__author__ = 'Ziga Vucko'


import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error
from mllib import DataSaver, DataPreprocessor
import warnings
warnings.filterwarnings('error')


###########################################################################################
# REGRESSION
###########################################################################################
def mae(y_true, y_pred, log=False):
    """
    Calculate mean absolute error of predictions.
    """
    if log:
        print 'Mean absoulute error: %.3f' % mean_absolute_error(y_true, y_pred)
    else:
        return mean_absolute_error(y_true, y_pred)


def rmae(y_true, y_pred, log=False):
    """
    Calculate root mean absolute error of predictions.
    """
    if log:
        print 'Root mean absoulute error: %.3f' % np.sqrt(mae(y_true, y_pred))
    else:
        return np.sqrt(mae(y_true, y_pred))


def mse(y_true, y_pred, log=False):
    """
    Calculate mean squared error of predictions.
    """
    if log:
        print 'Mean squared error: %.3f' % mean_squared_error(y_true, y_pred)
    else:
        return mean_squared_error(y_true, y_pred)


def rmse(y_true, y_pred, log=False):
    """
    Calculate root mean squared error of predictions.
    """
    if log:
        print 'Root mean squared error: %.3f' % np.sqrt(mse(y_true, y_pred))
    else:
        return np.sqrt(mse(y_true, y_pred))


def regression_results(y_true, y_pred, log=False):
    """
    Return all regression metrics results in the following order: MAE, RMAE, MSE, RMSE.
    """
    if log:
        print 'Mean absoulute error: %.2f' % mae(y_true, y_pred)
        print 'Root mean absoulute error: %.2f' % rmae(y_true, y_pred)
        print 'Mean squared error: %.2f' % rmse(y_true, y_pred)
        print 'Root mean squared error: %.2f' % rmse(y_true, y_pred)
    else:
        return mae(y_true, y_pred), rmae(y_true, y_pred), mse(y_true, y_pred), rmse(y_true, y_pred)
###########################################################################################


###########################################################################################
# CLASSIFICATION
###########################################################################################
def confusion_matrix(y_true, y_pred, classes=None, log=False):
    """
    Calculate the confusion matrix of predictions.
    """
    # encode labels to numeric format, if classes are not given
    if classes is None:
        y_true, y_pred, classes = DataPreprocessor.encode_labels(y_true, y_pred)

    n_classes = len(classes)
    mat = np.zeros(shape=(n_classes, n_classes), dtype=int)

    for i in range(len(y_true)):
        mat[int(y_pred[i]), int(y_true[i])] += 1

    if log:
        print 'Confusion matrix (predicted / true):'

        classes = classes.values()
        maxlen = max(len(str(c)) for c in classes)
        maxlen = maxlen if maxlen <= 16 else 16
        f1 = '%' + str(maxlen-3) + 's...'
        f2 = '%' + str(maxlen) + 's'
        f3 = '%' + str(maxlen) + 'd'

        # head
        head = (maxlen * ' ') + ' | '
        print head,
        count = len(head)

        # horizontal labels
        for c in classes:
            if len(str(c)) > maxlen:
                val = f1 % c[:(maxlen-3)]
            else:
                val = f2 % c
            count += len(val) + 1
            print val,
        print

        print (count * '-')

        # vertical labels & values
        for i in range(mat.shape[0]):
            c = classes[i]
            if len(str(c)) > maxlen:
                val = f1 % c[:(maxlen-3)]
            else:
                val = f2 % c
            print val + ' | ',
            for j in range(mat.shape[1]):
                print f3 % mat[i, j],
            print ' '
    else:
        return mat, classes


def ca(y_true, y_pred, log=False):
    """
    Calculate classification accuracy of predictions.
    """
    correct, all = 0, len(y_true)
    for i in range(all):
        if y_true[i] == y_pred[i]:
            correct += 1
    ca_ = float(correct) / all
    if log:
        print 'Classification accuracy: %.2f (%d/%d)' % (ca_, correct, all)
    else:
        return ca_, correct, all


def precision(y_true, y_pred, per_class=False, log=False):
    """
    Calculate precision of predictions for each class.
    """
    mat, classes = confusion_matrix(y_true, y_pred)
    vals, counts, avg = {}, {}, 0
    sum_cols = mat.sum(axis=0, dtype=float)
    sum_rows = mat.sum(axis=1, dtype=float)
    for i in range(mat.shape[0]):
        try:
            vals[classes[i]] = mat[i, i] / sum_rows[i]
        except RuntimeWarning:
            vals[classes[i]] = 0
        counts[classes[i]] = (mat[i, i], sum_rows[i])
        avg += vals[classes[i]] * sum_cols[i]
    avg /= len(y_true)

    if log:
        if per_class:
            print 'Precision:'
            for (label, val) in vals.iteritems():
                print '\t%s: %.2f (%d/%d)' % (str(label), val, counts[label][0], counts[label][1])
            print '->avg: %.2f' % avg
        else:
            print 'Precision: %.2f' % avg
    else:
        if per_class:
            return vals, avg
        else:
            return avg


def recall(y_true, y_pred, per_class=False, log=False):
    """
    Calculate recall of predictions for each class.
    """
    mat, classes = confusion_matrix(y_true, y_pred)
    vals, counts, avg = {}, {}, 0
    sum_cols = mat.sum(axis=0, dtype=float)
    for i in range(mat.shape[1]):
        try:
            vals[classes[i]] = mat[i, i] / sum_cols[i]
        except RuntimeWarning:
            vals[classes[i]] = 0
        counts[classes[i]] = (mat[i, i], sum_cols[i])
        avg += vals[classes[i]] * sum_cols[i]
    avg /= len(y_true)

    if log:
        if per_class:
            print 'Recall:'
            for (label, val) in vals.iteritems():
                print '\t%s: %.2f (%d/%d)' % (str(label), val, counts[label][0], counts[label][1])
            print '->avg: %.2f' % avg
        else:
            print 'Recall: %.2f' % avg
    else:
        if per_class:
            return vals, avg
        else:
            return avg


def fscore(y_true, y_pred, beta=1, per_class=False, log=False):
    """
    Calculate F-score of predictions for each class. Parameter beta specifies the type of F-score.
    """
    mat, classes = confusion_matrix(y_true, y_pred)
    prec, _ = precision(y_true, y_pred, per_class=True)
    rec, _ = recall(y_true, y_pred, per_class=True)

    vals, avg = {}, 0
    sum_cols = mat.sum(axis=0, dtype=float)
    for i in range(mat.shape[0]):
        try:
            vals[classes[i]] = (1 + beta**2) * ((prec[classes[i]] * rec[classes[i]]) / ((beta**2 * prec[classes[i]]) + rec[classes[i]]))
        except RuntimeWarning:
            vals[classes[i]] = 0
        avg += vals[classes[i]] * sum_cols[i]
    avg /= len(y_true)

    if log:
        if per_class:
            print 'F-%d score:' % beta
            for (label, val) in vals.iteritems():
                print '\t%s: %.2f' % (label, val)
            print '->avg: %.2f' % avg
        else:
            print 'F-%d score: %.2f' % (beta, avg)
    else:
        if per_class:
            return vals, avg
        else:
            return avg


def classification_results(y_true, y_pred, per_class=False, log=False):
    """
    Return all classification metrics results in the following order: classification accuracy precision, recall, F-1
    score. Results for precision, recall and F1-score are given for each separate class.
    """
    if log:
        ca(y_true, y_pred, log=log)
        if per_class:
            print
        precision(y_true, y_pred, per_class=per_class, log=log)
        if per_class:
            print
        recall(y_true, y_pred, per_class=per_class, log=log)
        if per_class:
            print
        fscore(y_true, y_pred, per_class=per_class, log=log)
        if per_class:
            print
        confusion_matrix(y_true, y_pred, log=log)
    else:
        return ca(y_true, y_pred), \
               precision(y_true, y_pred, per_class=per_class), \
               recall(y_true, y_pred, per_class=per_class), \
               fscore(y_true, y_pred, per_class=per_class), \
               confusion_matrix(y_true, y_pred)


def classification_results_to_csv(y_true, y_pred, classes=None, path_confusion_matrix=None, path_metrics=None):
    """
    Write classification results to csv file. Save confusion matrix (rows: predicted, cols: true) to file specified by
    path_confusion_matrix and other measures (counts, classification accuracy, precision, recall, F1-score) to file
    specified by path_metrics). Parameter classes has to be a dictionary, where keys are numbers and values are labels.
    """
    # set paths
    if path_confusion_matrix is None:
        path_confusion_matrix = 'results/confusion_matrix.csv'
    if path_metrics is None:
        path_metrics = 'results/metrics.csv'

    # prepare data for confusion matrix
    mat, classes = confusion_matrix(y_true, y_pred, classes)
    classes = classes.values()

    # save confusion matrix
    DataSaver.save(path_confusion_matrix,
                   x=mat,
                   top=['predicted\\true'] + classes,
                   left=classes)

    # prepare data for other measures
    ca_, n_pred, n_true = ca(y_true, y_pred)
    prec, prec_avg = precision(y_true, y_pred, per_class=True)
    rec, rec_avg = recall(y_true, y_pred, per_class=True)
    f, f_avg = fscore(y_true, y_pred, per_class=True)

    # create matrix of measures
    mat = np.vstack((np.array(['' for _ in range(len(classes))], dtype=str),
                     np.array(prec.values(), dtype=str),
                     np.array(rec.values(), dtype=str),
                     np.array(f.values(), dtype=str))).T

    # add means
    left = classes + ['Total means']
    means = np.array(['%f (%d/%d)' % (ca_, n_pred, n_true), prec_avg, rec_avg, f_avg], dtype=str)
    mat = np.vstack((mat, means))

    # save metrics results
    DataSaver.save(path_metrics,
                   x=mat,
                   top=['', 'Classification accuracy', 'Precision', 'Recall', 'F1-score'],
                   left=left)
###########################################################################################


def get_metric_func(metric=None):
    """
    Function return a pointer to a function specified by parameter metric.
    """
    if metric == 'mae':
        return mae
    elif metric == 'rmae':
        return rmae
    elif metric == 'mse':
        return rmse
    elif metric == 'rmse':
        return rmse
    elif metric == 'ca':
        return ca
    elif metric == 'precision':
        return precision
    elif metric == 'recall':
        return recall
    elif metric == 'fscore':
        return fscore
    else:
        return None
